# tadirah_rdf

RDF/XML embodiment of the TaDiRAH taxonomy of digital research activities in the humanities. 
Based on TaDiRAH version 0.5.1, 05/2014

The purpose of this project is to enable programmatic import of TaDiRAH into a semantic system, e.g. Semantic MediaWIki.


I am not involved in TaDiRAH development per se. For information about the authors of TaDiRAH, pleae see: http://tadirah.dariah.eu/